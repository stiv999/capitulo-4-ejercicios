#Autor: Steven Gumanan Alejandro          #steven.guaman@unl.edu.ec
'''¿Qué mostrará en pantalla el sigueinte programa de Python? '''


def fred():
    print('Zap', end= ' ')
                                #EL 'end'  es para que al presentar (print), no salete de linea.
def jane():
    print ('ABC', end= ' ')

jane()
fred()
jane()

'''ABC-Zap-ABC'''