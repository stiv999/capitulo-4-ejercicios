#Autor: Steven Gumanan Alejandro          #steven.guaman@unl.edu.ec
'''Rescribir el programa del salario y crea una función'''

#EJERCICIO DEL CAPITULO 2
'''
print ('BIENVENIDO\n vamos a calcular un salario')
h_t = float (input('Ingrese el número de horas trabajadas:'))
t_h = float (input ('Ingrese la tarifa por hora:'))
reslt = h_t * t_h
print (f'El salario total es de: {reslt:.2f} $')
'''
#EJERCICIO DEL CAPITULO 3
'''
print ('BIENVENIDO\n vamos a calcular un salario')

try:
    h_t = float(input('Ingrese el número de horas trabajadas:'))
    t_h = float(input('Ingrese la tarifa por hora:'))

    reslt = h_t * t_h
    if h_t > 0  and h_t <= 40:
        print(f'El salario total es de: {reslt:.2f} $')
    elif h_t >40:
        h_e = (h_t-40)
        print(f'El salario normal es de: {reslt:.2f} $')
        print ('horas extra: ', h_e)
        print ('salario de horas extra: ', (h_e*1.5))
        reslt = reslt + (h_e*1.5)
        print(f'El salario total es de: {reslt:.2f} $')
    else:
        print ('Por favor introduzca un número positivo')
except:
    print('Por favor introduzca un número')
'''

#EJERCICIO DEL CAPITULO 4
print ('BIENVENIDO\n vamos a calcular un salario')

try:
    h_t = float(input('Ingrese el número de horas trabajadas:'))
    t_h = float(input('Ingrese la tarifa por hora:'))
    reslt = h_t * t_h

    def calculo_salario(a, b):
        vh_e = b / 2
        return vh_e
    vh_e = calculo_salario(h_t, t_h)

    if h_t > 0  and h_t <= 40:
        print(f'El salario total es de: {reslt:.2f} $')
    elif h_t >40:
        h_e = (h_t-40)
        reslt = reslt + (h_e * vh_e)
        print(f'El salario total es de: {reslt:.2f} $')
    else:
        print ('Por favor introduzca un número positivo')
except:
    print('Por favor introduzca un número')